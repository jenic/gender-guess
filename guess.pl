#!/usr/bin/perl
# Gender Guesser rewritten from Javascript "Hacker Factor" Gender Guesser which
# is based on a 2003 research paper:
# http://www.cs.biu.ac.il/~koppel/papers/male-female-text-final.pdf
# Specific weight values taken from "Gender Genie"

sub _say ($);
sub calculate;

my ($word, $m_informal, $m_formal, $f_informal, $f_formal, $i);
$m_informal = $m_formal = $f_informal = $f_formal = 0;

# Positive = male, negative = female
my %dict_informal =
    ( actually => -49
    , am => -42
    , as => 37
    , because => -55
    , but => -43
    , ever => 21
    , everything => -44
    , good => 31
    , has => -73
    , him => -73
    , if => 25
    , in => 10
    , is => 19
    , like => -43
    , more => -41
    , now => 33
    , out => -39
    , since => -25
    , so => -64
    , some => 58
    , something => 26
    , the => 17
    , this => 44
    , too => -38
    , well => 15
    );

my %dict_formal =
    ( a => 6
    , above => 4
    , and => -4
    , are => 28
    , around => 42
    , as => 23
    , at => 6
    , be => -17
    , below => 8
    , her => -9
    , hers => -3
    , if => -47
    , is => 8
    , it => 6
    , many => 6
    , me => -4
    , more => 34
    , myself => -4
    , not => -27
    , said => 5
    , she => -6
    , should => -7
    , the => 7
    , these => 8
    , to => 2
    , was => -1
    , we => -8
    , what => 35
    , when => -17
    , where => -18
    , who => 19
    , with => -52
    , your => -17
    );

while (<>) {
    # Normalize input
    my @words = split(/\s/, lc);
    next if (@words < 1);

    _say "Have less than 300 words: " . @words
        if (@words < 300);

    for (@words) {
        if (exists $dict_informal{$_}) {
            $m_informal += $dict_informal{$_}
                if ($dict_informal{$_} > 0);
            $f_informal -= $dict_informal{$_}
                if ($dict_informal{$_} < 0);
        }

        if (exists $dict_formal{$_}) {
            $m_formal += $dict_formal{$_}
                if ($dict_formal{$_} > 0);
            $f_formal -= $dict_formal{$_}
                if ($dict_formal{$_} < 0);
        }
    }
}

# Display results
END {
    my ($genre, $gender, $percent, $diff);
    my $out = "Genre: %s // Female %i Male %i Diff %i (%i%%) Verdict %s$/";

    $genre = "Informal";
    $diff = ($m_informal - $f_informal);
    $percent = calculate($m_informal, $f_informal);
    $gender = ($m_informal > $f_informal) ? 'Male' : 'Female';

    printf $out,
        $genre, $f_informal, $m_informal, $diff, $percent, $gender;

    # formal
    $genre = "Formal";
    $diff = ($m_formal - $f_formal);
    $percent = calculate($m_formal, $f_formal);
    $gender = ($m_formal > $f_formal) ? 'Male' : 'Female';

    printf $out,
        $genre, $f_formal, $m_formal, $diff, $percent, $gender;
}

sub _say ($) {
    warn "$_[0]", $/;
}

sub calculate {
    my $percent = (($_[0] + $_[1] > 0) ?
        ($_[0] * 100 / ($_[0] + $_[1])) : 0);

    _say "Weak emphasis could indicate European."
        if (($percent > 40) && ($percent < 60));

   return $percent;
}
